
    const gREQUEST_STATUS_OK = 200;
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

    // Mẫu gọi API lấy thời tiết hiện tại
    function getCurrentWeather(city) {
        // var city = "Ha noi";

        if (city != '') {

            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + "&units=metric" +
                    "&APPID=c10bb3bd22f90d636baa008b1529ee25",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    
                    var vResult = 
                        `<h2 class="text-center">Current Weather for ${data.name}, ${data.sys.country}</h2><br>
                        <b>Weather</b>: ${data.weather[0].main}<br>
                        <b>Img</b>: <img src="http://openweathermap.org/img/w/${data.weather[0].icon}.png" alt="weather"><br>
                        <b>Description</b>: ${data.weather[0].description}<br>
                        <b>Temperature</b>: ${data.main.temp}<br>
                        <b>Pressure</b>: ${data.main.pressure}<br>
                        <b>Humidity</b>: ${data.main.humidity}<br>
                        <b>Min Temperature</b>: ${data.main.temp_min}<br>
                        <b>Max Temperature</b>: ${data.main.temp_max}<br>
                        <b>Wind Speed</b>: ${data.wind.speed}<br>
                        <b>Wind Direction</b>: ${data.wind.deg}<br>
                        `;
                    
                    $("#show-result").html(vResult);
                },
                error: function (ajaxContext) {
                    $("#show-result").html("Du lieu chua co, hay thu lai.");
                  }
            });
        } else {
            console.error("City not valid");
            $("#show-result").html("Hay nhap thanh pho.");
        }
    }

    // Mẫu gọi API lấy dữ liệu dự báo thời tiết
    function getForecast(city, days) {
        // var city = "Ha noi";
        // var days = 1;

        if (city != '' && days != '') {
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + city + "&units=metric" +
                    "&cnt=" + days + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var vTableElm = $("#table-data");
                    var vTbody = vTableElm.find("tbody");
                    vTbody.find("tr").remove();
                    for (var i = 0; i < data.list.length; i++) {
                        var vRow = $("<tr>").appendTo(vTbody);
                        $("<td>",{html:(i+1)}).appendTo(vRow);
                        var icon = "http://openweathermap.org/img/w/" + data.list[i].weather[0].icon + ".png";
                        $("<td>",{html:"<img src='"+icon+"'>"}).appendTo(vRow);
                        var weather = data.list[i].weather[0].main;
                        $("<td>",{html: weather}).appendTo(vRow);
                        var description = data.list[i].weather[0].description;
                        $("<td>",{html: description}).appendTo(vRow);
                        var morning_temperature = data.list[i].temp.morn;
                        $("<td>",{html: morning_temperature}).appendTo(vRow);
                        var night_temperature = data.list[i].temp.night;
                        $("<td>",{html: night_temperature}).appendTo(vRow);
                        var min_temperature = data.list[i].temp.min;
                        $("<td>",{html: min_temperature}).appendTo(vRow);
                        var max_temperature = data.list[i].temp.max;
                        $("<td>",{html: max_temperature}).appendTo(vRow);
                        var pressure = data.list[i].pressure;
                        $("<td>",{html: pressure}).appendTo(vRow);
                        var humidity = data.list[i].humidity;
                        $("<td>",{html: humidity}).appendTo(vRow);
                        var speed = data.list[i].speed;
                        $("<td>",{html: speed}).appendTo(vRow);
                        var deg = data.list[i].deg;
                        $("<td>",{html: deg}).appendTo(vRow);
                    }
                }
            });
        } else {
            console.error("Input not valid")
        }
    }
